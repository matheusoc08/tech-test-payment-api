using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly PaymentContext _context;

        public VendaController(PaymentContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult RegistrarVenda(Venda venda)
        {
            var vendedor = _context.Vendedores.Find(venda.Vendedor.Id);
            if (vendedor == null)
            {
                return NotFound("Vendedor não encontrado.");
            }

            if (venda.Item.Count == 0)
            {
                return BadRequest("A lista de itens não pode estar vazia!");
            }

            venda.Vendedor = vendedor;
            venda.Status = EnumStatusVenda.AguardandoPagamento;
            _context.Vendas.Add(venda);
            _context.SaveChanges();

            return Ok(venda);
        }

        [HttpGet("{id}")]
        public IActionResult BuscarPorId(int id)
        {
            if (_context.Vendas.Find(id) == null)
            {
                return NotFound("Venda não localizada pelo Id informado.");
            }

            var vendaBanco = _context.Vendas
                .Where(venda => venda.Id == id)
                .Select(venda => new
                {
                    venda.Id,
                    venda.Data,
                    status = venda.Status.ToString(),
                    vendedor = venda.Vendedor,
                    item = venda.Item
                }
            );

            return Ok(vendaBanco);
        }

        [HttpPut("{id}")]
        public IActionResult EditarVenda(int id, Venda venda)
        {
            Venda vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
            {
                return NotFound("Venda não localizada pelo Id informado.");
            }

            if (((int)venda.Status) > 4)
            {
                return BadRequest("Status inválido.");
            }

            if (VerificaStatus(vendaBanco.Status, venda.Status).Equals(vendaBanco.Status))
            {
                return BadRequest($"Status {vendaBanco.Status} não pode ser atualizado para {venda.Status}.");
            }

            vendaBanco.Status = venda.Status;
            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();
            return Ok(BuscarPorId(id));
        }

        private EnumStatusVenda VerificaStatus(EnumStatusVenda statusAtual, EnumStatusVenda statusNovo)
        {
            if (statusAtual.Equals(EnumStatusVenda.AguardandoPagamento) &&
                (statusNovo.Equals(EnumStatusVenda.PagamentoAprovado) || statusNovo.Equals(EnumStatusVenda.Cancelada)))
            {
                return statusNovo;
            }

            if (statusAtual.Equals(EnumStatusVenda.PagamentoAprovado) &&
                statusNovo.Equals(EnumStatusVenda.EnviadoParaTransportadora) || statusNovo.Equals(EnumStatusVenda.Cancelada))
            {
                return statusNovo;
            }

            if (statusAtual.Equals(EnumStatusVenda.EnviadoParaTransportadora) &&
                statusNovo.Equals(EnumStatusVenda.Entregue))
            {
                return statusNovo;
            }

            return statusAtual;
        }
    }
}