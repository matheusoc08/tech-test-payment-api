using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly PaymentContext _context;

        public VendedorController(PaymentContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult CadastrarVendedor(Vendedor vendedor)
        {
            _context.Vendedores.Add(vendedor);
            _context.SaveChanges();
            return Ok(vendedor);
        }

        [HttpGet]
        public IActionResult BuscarTodos()
        {
            List<Vendedor> vendedorBanco = _context.Vendedores.ToList();

            if (vendedorBanco.Count == 0)
            {
                return NotFound("Não há vendedores cadastrados.");
            }

            return Ok(vendedorBanco);
        }

        [HttpGet("{id}")]
        public IActionResult BuscarPorId(int id)
        {
            Vendedor vendedorBanco = _context.Vendedores.Find(id);

            if (vendedorBanco == null)
            {
                return NotFound("Vendedor não encontrado");
            }

            return Ok(vendedorBanco);
        }

        [HttpPut("{id}")]
        public IActionResult EditarVendedor(int id, Vendedor vendedor)
        {
            Vendedor vendedorBanco = _context.Vendedores.Find(id);

            if (vendedorBanco == null)
            {
                return NotFound("Vendedor não encontrado");
            }

            vendedorBanco.Nome = vendedor.Nome;
            vendedorBanco.Cpf = vendedor.Cpf;
            vendedorBanco.Email = vendedor.Email;
            vendedorBanco.Telefone = vendedor.Telefone;

            _context.Update(vendedorBanco);
            _context.SaveChanges();
            return Ok(vendedorBanco);
        }

    }
}